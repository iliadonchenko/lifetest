package main

import (
	"fmt"
	"net/http"
	"time"
	"strconv"
	"flag"
	"log"
	"github.com/gorilla/mux"
	"encoding/json"
)


type Data struct {
	DataInfo   []Stream `json:"data"`
}

type Stream struct {
	Type string `json:"type"`
	ID string  `json:"id"`
	State string `json:"state"`
	Timer *time.Timer `json:"-"`
	Attributes `json:"attributes"`
}

type Attributes struct {
	Created string `json:"created"`
}

type Error struct {
	ErrorInfo `json:"errors"`
}

type ErrorInfo struct {
		Message string `json:"detail"`
}


const (
	appName = "lifetest" // название сервиса
	version = "0.1" // версия
	date    = "2017-08-10" // дата сборки
)
const apipath = "/api/v1" //путь к api

var stream_map = make(map[string]*Stream)

func main() {

	log.Println(appName, version, date)
	port := flag.String("port", "5000", "application port")
	timeout := flag.Int("timeout", 10, "timeout (seconds)")
	flag.Parse()

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc(apipath + "/streams", getStatus).Methods("GET") //Список трансляций
	router.HandleFunc(apipath + "/streams", addStream).Methods("POST") //Добавление трансляции
	router.HandleFunc(apipath + "/streams/{id:[0-9]+}/start", func(w http.ResponseWriter, r *http.Request) {
		startStream(w, r, *timeout)
	}) //Старт трансляции
	router.HandleFunc(apipath + "/streams/{id:[0-9]+}/stop", stopStream) //Остановка трансляции

	log.Println("Port:", *port)

	http.ListenAndServe(":" + *port, router)
}

func getStatus(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/vnd.api+json")

	streams := []Stream{}

	if len(stream_map) != 0 { //Проверяем есть ли вообще трансляции
		for _, stream := range stream_map {
			streams = append (streams, *stream)
		}

		m := Data{DataInfo: streams}

		b, err := json.Marshal(m)
		if err != nil {
			printError(w,"Error marshalling responce")
		} else {
			fmt.Fprintln(w, string(b))
		}
	} else {
		printError(w,"No streams found")
	}
}

func addStream(w http.ResponseWriter, r *http.Request) { //Создаем новую трансляцию
	w.Header().Set("Content-Type", "application/vnd.api+json")
	t := time.Now()
	id := len(stream_map)+1
	stream_map[strconv.Itoa(id)] = &Stream{State: "Created", Type: "stream", ID: strconv.Itoa(id), Attributes: Attributes{Created: t.Format("2006-01-02T15:04:05Z07:00")}}
	fmt.Fprintf(w, "Stream %d added!", id)
	log.Println("Stream added:", id)
}

func startStream(w http.ResponseWriter, r *http.Request, timeout int) {
	w.Header().Set("Content-Type", "application/vnd.api+json")

	vars := mux.Vars(r)
	id := vars["id"]

	switch {
	case stream_map[id] == nil:
		printError(w, fmt.Sprintf("No such stream: %s", id))
	case stream_map[id].State == "Created":
		stream_map[id].State = "Active"
		go waitTimeout(id, timeout)
	case stream_map[id].State == "Interrupted":
		stream_map[id].State = "Active"
		stream_map[id].Timer.Reset(time.Second * time.Duration(timeout))
		log.Println("Stream restarted:", id)
	default:
		printError(w, fmt.Sprintf("Can't start stream %s (state %s)", id, stream_map[id].State))
	}
}

func stopStream(w http.ResponseWriter, r *http.Request) { //Остановка стрима
	w.Header().Set("Content-Type", "application/vnd.api+json")

	vars := mux.Vars(r)
	id := vars["id"]

	switch {
	case stream_map[id] == nil:
		printError(w, fmt.Sprintf("No such stream: %s", id))
	case stream_map[id].State == "Active":
		stream_map[id].State = "Interrupted"
		log.Println("Stream interrupted:", id)
	default:
		printError(w, fmt.Sprintf("Stream %s is not active, can't interrupt!", id))
	}
}

func waitTimeout(id string, timeout int) { //Ждем окончания трансляции
	log.Println("Stream started:", id)
	stream_map[id].Timer = time.NewTimer(time.Second * time.Duration(timeout))
	<- stream_map[id].Timer.C
	stream_map[id].State = "Finished"
	log.Println("Stream finished:", id)
}

func printError(w http.ResponseWriter, msg string) { //Вывод ошибок
	ei := ErrorInfo{Message: msg}
	er := Error{ei}
	b, err := json.Marshal(er)
	if err != nil {
		log.Println("Error marshalling error message")
	} else {
		fmt.Fprintln(w, string(b))
	}
}
